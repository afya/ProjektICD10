Projekt ICD10
---------------------
Dostępne słowniki ICD 10 są złe - pomieszane formy, pomieszane liczby, brakuje informacji jak brzmi główne rozpoznanie

Nie jestem w stanie sam zrobić poprawek szybko i łatwo na 15 tysiącach rekordów, stąd pomysł na ten projekt.
W zamian oferuję - użytkowanie za free pliku wynikowego
Obecny plik został znaleziony w sieci, i nie wiem, kto ma do niego prawa autorskie.
Częściowo poprawiłem już powyżej 1000 kodów
Nie odpowiadam za prawdziwość i poprawność danych zawartych w tym pliku.


W pliku ICD10.csv zamieściłem nazwy kodów 4. znakowych ICD 10 wraz z opisem dla tego kodu.
Format pliku jest następujący (kodowanie UTF-8):

KOD_ICD10#opis_kodu_icd10

opisy zostały zapisane jedynie jako małe litery; programowo zawsze można zrobić z nich wielkie lub tylko 1. literę.

plik można zaimportować do programu OpenOffice/LibreOffice/Excel, celem łatwiejszej obróbki.
Przy zapisie do formatu CSV należy wybrać:
separator pola: #
separator ciągu "
kodowanie UTF-8


Jakich poprawek oczekuję po projekcie
- zamiana na wielkie litery nazw własnych:
np. Staphyloccosus aureus itp., Vibro cholerae
- dodanie fraz np. w kodach "C" tzn:
	jeżeli spojrzymy na rozpozanie  C44.0 - mamy tam napis:
C44.0#skóra wargi


brakuje, informacji, czym jest to rozpoznanie, moja wizja:
dodanie części z kodu C44, czyli inne nowotwory złośliwe skóry
po poprawce wartość przybrałaby postać:

C44.0#inne nowotwory złośliwe skóry: skóra wargi

 albo lepiej, robiąc wszsytko w mianowniku, l. pojedyncza:

C44.0#inny nowotwór złośliwy skóry: skóra wargi



Ok, jak się zabrać do pracy:

należy zainstalować sobie GIT

:dla Windows :
pięknie wyglądający program okienkowy: 
https://git-scm.com/download/gui/windows

dla Linuxa: 
#apt-get install git

adres projektu:
git@gitlab.com:afya/ProjektICD10.git
lub
https://gitlab.com/afya/ProjektICD10.git

wchodzimy sobie do dowolnego katalogu, gdzie chcemy uruchomić projekt
wpisujemy

git clone https://gitlab.com/afya/ProjektICD10.git

i prawie gotowe...

kiedy poprawimy plik i chcemy wysłać go innym  - wchodzimy do naszego katalogu projektu i  wpisujemy

git pull https://gitlab.com/afya/ProjektICD10.git master              (co oznacza: pobierz i "wpasuj" zdalne repozytorium w gałęzi master"
git add .  
git commit -m "Poprawka przez Jana Kowalskiego jan@wp.pl 2017-11-05"
git push origin master
                      (co oznacza: wypchnij na serwer, do gałęzi master)


aaa! zapomniałem, żeby wysłać nową wersję, trzeba mieć konto na gitlab.com
takie konto można za free założyć tutaj: https://gitlab.com/users/sign_in )



Zachęcam do współpracy! M. Chuć

marcin     a     afya.pl  (a zamień na at, czyli @)






